class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :name
      t.string :eventtype
      t.datetime :eventdate
      t.datetime :rsvpdate

      t.timestamps null: false
    end
  end
end
