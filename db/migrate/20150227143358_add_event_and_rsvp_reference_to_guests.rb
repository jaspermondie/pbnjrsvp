class AddEventAndRsvpReferenceToGuests < ActiveRecord::Migration
  def change
    add_reference :guests, :event, index: true
    add_reference :guests, :rsvp, index: true
  end
end
