class CreateGuests < ActiveRecord::Migration
  def change
    create_table :guests do |t|
      t.string :firstname
      t.string :lastname
      t.text :dietaryreq
      t.boolean :attending

      t.timestamps null: false
    end
  end
end
