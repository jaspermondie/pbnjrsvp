class AddGuestlimitToRsvp < ActiveRecord::Migration
  def change
    add_column :rsvps, :guestlimit, :integer
  end
end
