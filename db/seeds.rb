# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
#

User.create!(
  name: 'Test User',
  email: 'test@example.com',
  password: 'password'
)

event = Event.create!(
  name: 'Jane and John\'s Wedding',
  eventtype: 'Wedding',
  eventdate: DateTime.new(2015, 9, 1),
  rsvpdate: DateTime.new(2015, 4, 1)
)

50.times do
  generated_code = Rsvp.generate_code
  guestlimit = rand(1..4)

  rsvp = Rsvp.create!(code: generated_code,
                      guestlimit: guestlimit)

  rand(1..guestlimit).times do
    first_name  = Faker::Name.first_name
    last_name = Faker::Name.last_name
    guest = event.guests.create!(
      firstname: first_name,
      lastname: last_name
    )
    guest.rsvp = rsvp
    guest.save
  end
end
