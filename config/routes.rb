Rails.application.routes.draw do #  root 'welcome#index'
  root 'static_pages#home'

  get 'guests/not_linked', to: 'guests#guests_not_linked'
  get 'guests/linked', to: 'guests#guests_linked'
  get 'guests/responded', to: 'static_pages#guests_responded'
  get 'guests/export', to: 'guests#export'
  get 'guests/browse_file', to: 'guests#browse_file'
  get 'rsvps/replycards-pdf', to: 'rsvps#replycards_pdf'
  get 'rsvps/:code/replycard', to: 'rsvps#replycard', as: :replycard
  post 'guests/import', to: 'guests#import'
  post 'rsvps/search', to: 'rsvps#search'
  patch 'guests/generate_rsvp', to: 'guests#create_rsvp_code_for_guests'
  patch 'guests/unlink_guests', to: 'guests#unlink_guests'

  get 'login'   => 'sessions#new'
  post 'login'   => 'sessions#create'
  delete 'logout'  => 'sessions#destroy'

  resources :rsvps, param: :code do
    match 'guests/edit', to: 'guests#edit_all', as: :edit_guests, via: :get
    match 'guests/edit', to: 'guests#update_all', as: :update_guests, via: :patch
    resources :guests
  end

  resources :events
  resources :guests
end
