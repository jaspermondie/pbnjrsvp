require 'test_helper'

class GuestsControllerTest < ActionController::TestCase
  setup do
    @guest = guests(:john)
    @guest_johns_bro = guests(:johns_brother)
    @guest_jane = guests(:jane)
    @rsvp = rsvps(:rsvp_one)

    @match_warning_message = "We can't find a match for the lastname and RSVP Code"

    @user = users(:john)
    log_in_as(@user)
  end

  test 'export when get export then data should be sent' do
    # arrange and act
    get :export

    # assert
    assert logged_in?
    assert_response :success
    assert_equal 'text/csv', @response['Content-Type']
  end

  test 'import from csv when posted' do
    # arrange
    @file = fixture_file_upload('files/import_guests.csv')

    # act
    post :import, file: @file

    # assert
    assert logged_in?
    assert_redirected_to guests_path
  end

  test 'import from csv with some invalid data when posted' do
    # arrange
    @file = fixture_file_upload('files/import_guests_invalid_data.csv')

    # act
    post :import, file: @file

    # assert
    assert logged_in?
    assert_redirected_to guests_path
  end

  test 'should update single guest for rsvp code' do
    patch :update_all,
          rsvp_code: @rsvp.code,
          guests: { @guest.id => { attending: true  } }
    assert_response :success
  end

  test 'should update multiple guests for rsvp code' do
    patch :update_all, rsvp_code: @rsvp.code,
                       guests:
      { @guest.id => { attending: true, dietaryreq: 'a dietary req' },
        @guest_johns_bro.id => { attending: false, dietaryreq: 'see food diet' } }
    assert_redirected_to guests_responded_path
  end

  test 'should fail to update multiple guests with different rsvp code' do
    patch :update_all, rsvp_code: @rsvp.code,
                       guests:
      { @guest.id => { attending: true, dietaryreq: 'a dietary req' },
        @guest_jane.id => { attending: false, dietaryreq: 'see food diet' } }

    assert_template :edit_all
  end

  test 'should get edit guests for rsvp code with lastname' do
    get :edit_all, rsvp_code: @rsvp.code, lastname: @guest.lastname
    assert_response :success
  end

  test 'when edit guests with guest that has already responded,' \
    'should go redirect to root' do
    # arrange
    expected_warning_message = "It looks like you've already responded to our RSVP"
    guest = guests(:liz)
    rsvp = rsvps(:rsvp_30_rock)

    # act
    get :edit_all, rsvp_code: rsvp.code, lastname: guest.lastname

    # assert
    assert_redirected_to root_url
    assert_equal expected_warning_message, flash[:warning]
  end

  test 'when edit guests with invalid lastname as verifier,' \
    'should go redirect to root' do
    get :edit_all, rsvp_code: @rsvp.code, lastname: 'invalid-lastname1213'
    assert_redirected_to root_url
    assert_equal @match_warning_message, flash[:warning]
  end

  test 'when edit guests with blank lastname as verifier,' \
    'should go redirect to root' do
    get :edit_all, rsvp_code: @rsvp.code, lastname: ''
    assert_redirected_to root_url
    assert_equal @match_warning_message, flash[:warning]
  end

  test 'should get guests for rsvp code' do
    get :index, rsvp_code: @rsvp.code
    assert logged_in?
    assert_response :success
    assert_not_nil assigns(:guests)
  end

  test 'should get index' do
    get :index
    assert logged_in?
    assert_response :success
    assert_not_nil assigns(:guests)
  end

  test 'should get new' do
    get :new
    assert logged_in?
    assert_response :success
  end

  test 'should create guest' do
    assert logged_in?
    assert_difference('Guest.count') do
      post :create,
           guest: { firstname: @guest.firstname, lastname: @guest.lastname }
    end
  end

  test 'when creating guest with invalid data' \
    'then should not create guest and return to creation' do
    # act
    post :create, guest: { lastname: nil }

    # assert
    assert logged_in?
    assert_template :new
  end

  test 'should show guest' do
    get :show, id: @guest
    assert logged_in?
    assert_response :success
  end

  test 'should get edit' do
    get :edit, id: @guest
    assert logged_in?
    assert_response :success
  end

  test 'should update guest' do
    patch :update, id: @guest, guest: { attending: true }
    assert logged_in?
    assert_redirected_to guest_path(assigns(:guest))
  end

  test 'when updating guest with invalid data' \
    'then should go back to edit guest page' do
    # act
    patch :update, id: @guest, guest: { lastname: '' }

    # assert
    assert logged_in?
    assert_template :edit
  end

  test 'should destroy guest' do
    assert_difference('Guest.count', -1) do
      delete :destroy, id: @guest
    end
    assert logged_in?
    assert_redirected_to guests_path
  end

  test 'guests_not_linked should get list of' \
        'guests with no rsvp' do
    # arrange
    # act
    get :guests_not_linked

    # assert
    assert logged_in?
    assert_response :success
  end

  test 'create_rsvp_code_for_guests when given' \
        'selected guests, should create rsvp and link them' do
    # arrange
    joe = guests(:joe)
    tracy = guests(:tracy)
    jenna = guests(:jenna)

    # act and assert
    assert logged_in?
    assert_difference('Rsvp.count', 1) do
      patch :create_rsvp_code_for_guests,
            guests: { joe.id => { selected: '1' },
                      tracy.id => { selected: '1' },
                      jenna.id => { selected: '0' }
                    }
    end
  end

  test 'create_rsvp_code_for_guests' \
    'when no selected guests, shouldnt create rsvp' do
    # arrange
    joe = guests(:joe)
    tracy = guests(:tracy)
    jenna = guests(:jenna)

    # act and assert
    assert logged_in?
    assert_difference('Rsvp.count', 0) do
      patch :create_rsvp_code_for_guests,
            guests: { joe.id => { selected: '0' },
                      tracy.id => { selected: '0' },
                      jenna.id => { selected: '0' }
                    }
    end
  end

  test 'create_rsvp_code_for_guests' \
       'when selected guests have rsvp code already shouldnt create rsvp' do
    # arrange
    liz = guests(:liz)
    tracy = guests(:tracy)
    jenna = guests(:jenna)

    # act and assert
    assert logged_in?
    assert_difference('Rsvp.count', 0) do
      patch :create_rsvp_code_for_guests,
            guests: { liz.id => { selected: '1' },
                      tracy.id => { selected: '1' },
                      jenna.id => { selected: '0' }
                    }
    end
  end

  test 'guests_linked should get list of' \
        'guests with rsvps' do
    # arrange
    # act
    get :guests_linked

    # assert
    assert logged_in?
    assert_response :success
  end

  test 'remove_rsvp_code_for_guests when given' \
        'selected guests, should remove link to rsvps' do
    # arrange
    guest_1 = guests(:john)
    guest_2 = guests(:jane)
    guest_3 = guests(:liz)

    message = 'Successfully removed RSVP from guests'

    # act
    assert logged_in?
    patch :unlink_guests,
          guests: { guest_1.id => { selected: '1' },
                    guest_2.id => { selected: '1' },
                    guest_3.id => { selected: '0' }
                  }
    # assert
    assert_redirected_to guests_linked_path
    assert_equal message, flash[:success]
  end

  test 'remove_rsvp_code_for_guests' \
    'when no selected guests, shouldnt remove rsvp' do
    # arrange
    guest_1 = guests(:john)
    guest_2 = guests(:jane)
    guest_3 = guests(:liz)

    message = 'Couldn\'t remove RSVP code the guests'

    # act
    assert logged_in?
    patch :unlink_guests,
          guests: { guest_1.id => { selected: '0' },
                    guest_2.id => { selected: '0' },
                    guest_3.id => { selected: '0' }
                  }
    # assert
    assert_not guest_1.rsvp_id.nil?
    assert_not guest_2.rsvp_id.nil?
    assert_not guest_3.rsvp_id.nil?

    assert_redirected_to guests_linked_path
    assert_equal message, flash[:warning]
  end

  test 'remove_rsvp_code_for_guests' \
       'when selected guests dont have rsvp code does nothing' do
    # arrange
    guest_1 = guests(:joe)
    guest_2 = guests(:tracy)
    guest_3 = guests(:jenna)

    message = 'Couldn\'t remove RSVP code the guests'

    # act
    assert logged_in?
    patch :unlink_guests,
          guests: { guest_1.id => { selected: '1' },
                    guest_2.id => { selected: '1' },
                    guest_3.id => { selected: '0' } }

    # assert
    assert guest_1.rsvp_id.nil?
    assert guest_2.rsvp_id.nil?
    assert guest_3.rsvp_id.nil?

    assert_redirected_to guests_linked_path
    assert_equal message, flash[:warning]
  end
end
