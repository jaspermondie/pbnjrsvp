require 'test_helper'

class RsvpsControllerTest < ActionController::TestCase
  setup do
    @guest = guests(:john)
    @rsvp = rsvps(:rsvp_one)

    @user = users(:john)
    log_in_as(@user)
  end

  test 'when having rsvp code and lastname' \
    'should search and redirect to results' do
    # arrange
    lastname = @guest.lastname
    code = @guest.rsvp.code

    # act
    post :search, code: code, lastname: lastname

    # assert
    assert_redirected_to rsvp_edit_guests_path(code, lastname: lastname)
  end

  test 'when having invalid rsvp code and lastname' \
    'should still redirect to edit page' do
    # arrange
    lastname = 'lastname'
    code = 'invalid-code'

    # act
    post :search, code: code, lastname: lastname

    # assert
    assert_redirected_to rsvp_edit_guests_path(code, lastname: lastname)
  end

  test 'should get index' do
    get :index
    assert logged_in?
    assert_response :success
    assert_not_nil assigns(:rsvps)
  end

  test 'should get new' do
    get :new
    assert logged_in?
    assert_response :success
  end

  test 'should create rsvp' do
    assert_difference('Rsvp.count') do
      post :create, rsvp: { code: @rsvp.code }
    end

    assert logged_in?
    assert_redirected_to rsvp_path(assigns(:rsvp))
  end

  test 'when creating rsvp with no code,' \
    'should fail to create rsvp and render new' do
    # act
    post :create, rsvp: { code: nil }

    # assert
    assert_template :new
    assert logged_in?
  end

  test 'should show rsvp' do
    get :show, code: @rsvp.code
    assert_response :success
    assert logged_in?
  end

  test 'should get edit' do
    get :edit, code: @rsvp.code
    assert_response :success
    assert logged_in?
  end

  test 'should update rsvp' do
    patch :update, code: @rsvp.code, rsvp: { code: @rsvp.code }
    assert_redirected_to rsvp_path(assigns(:rsvp))
    assert logged_in?
  end

  test 'when updating rsvp to have no code, should fail to update rsvp' do
    # act
    patch :update, code: @rsvp.code, rsvp: { code: nil }

    # assert
    assert_template :edit
    assert logged_in?
  end

  test 'should destroy rsvp' do
    assert_difference('Rsvp.count', -1) do
      delete :destroy, code: @rsvp.code
    end

    assert_redirected_to rsvps_path
    assert logged_in?
  end

  test 'reply card when getting reply card for an rsvp' do
    # arrange and act
    get :replycard, code: @rsvp.code

    # assert
    assert_response :success
    assert_equal 'application/pdf', @response['Content-Type']
    assert logged_in?
  end

  test 'reply cards all when getting requesting for pdfs' do
    # arrange and act
    get :replycards_pdf

    # assert
    assert_response :success
    assert_equal 'application/pdf', @response['Content-Type']
    assert logged_in?
  end
end
