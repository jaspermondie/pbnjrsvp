require 'test_helper'

class EventsControllerTest < ActionController::TestCase
  setup do
    @event = events(:one)
  end

  test 'should get index' do
    get :index
    assert_response :success
    assert_not_nil assigns(:events)
  end

  test 'should get new' do
    get :new
    assert_response :success
  end

  test 'should create event' do
    assert_difference('Event.count') do
      post :create,
           event: { eventdate: @event.eventdate,
                    eventtype: @event.eventtype,
                    name: @event.name,
                    rsvpdate: @event.rsvpdate }
    end
  end

  test 'when creating event with no name,' \
    'then should fail to create event and go to new event page' do
    # act
    post :create,
         event: { eventdate: @event.eventdate,
                  eventtype: @event.eventtype,
                  name: nil,
                  rsvpdate: @event.rsvpdate }

    # assert
    assert_template :new
  end

  test 'should show event' do
    get :show, id: @event
    assert_response :success
  end

  test 'should get edit' do
    get :edit, id: @event
    assert_response :success
  end

  test 'should update event' do
    patch :update,
          id: @event,
          event: { eventdate: @event.eventdate,
                   eventtype: @event.eventtype,
                   name: @event.name,
                   rsvpdate: @event.rsvpdate }
    assert_redirected_to event_path(assigns(:event))
  end

  test 'when updating event to have no name, should not update event' do
    # act
    patch :update,
          id: @event,
          event: { eventdate: @event.eventdate,
                   eventtype: @event.eventtype,
                   name: nil,
                   rsvpdate: @event.rsvpdate }

    # assert
    assert_template :edit
  end

  test 'should destroy event' do
    assert_difference('Event.count', -1) do
      delete :destroy, id: @event
    end

    assert_redirected_to events_path
  end
end
