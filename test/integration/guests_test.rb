require 'test_helper'

class GuestsTest < ActionDispatch::IntegrationTest
  test 'when going to guest page and not logged in,' \
       'should go to log in page' do
    get guests_path
    assert_redirected_to login_path
  end
end
