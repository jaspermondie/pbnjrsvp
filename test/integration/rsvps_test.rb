require 'test_helper'

class RsvpsTest < ActionDispatch::IntegrationTest
  test 'when going to rsvp page and not logged in,' \
       'should redirect to log in page' do
    get rsvps_path
    assert_redirected_to login_path
  end
end
