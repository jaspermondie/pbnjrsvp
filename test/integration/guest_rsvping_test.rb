require 'test_helper'
class GuestRsvpingTest < ActionDispatch::IntegrationTest
  test 'happy workflow for guest' do
    guest = guests(:john)

    get root_path
    assert_template '/'
    get rsvp_edit_guests_path guest.rsvp.code, lastname: guest.lastname
    assert_template 'guests/edit_all'
    patch rsvp_update_guests_path(guest.rsvp.code),
          guests: {
            id: {
              firstname: 'John',
              lastname: 'Smith',
              dietaryreq: 'MyText',
              attending: true,
              responded: true
            }
          }
    assert_response :success
  end
end
