require 'test_helper'

class RsvpTest < ActiveSupport::TestCase
  test 'generate_rsvp when called creates new rsvp' do
    # arrange
    # act
    rsvp = Rsvp.generate_rsvp

    # assert
    assert_not_nil rsvp
  end

  test 'generate_code when default length,
          should get code with length' do
    # arrange
    expected = 4

    # act
    code = Rsvp.generate_code
    actual = code.length

    # assert
    assert_equal expected, actual
  end

  test 'generate_code when given length
          should get code with correct length' do
    # arrange
    expected = 10

    # act
    code = Rsvp.generate_code(expected)
    actual = code.length

    # assert
    assert_equal expected, actual
  end
end
