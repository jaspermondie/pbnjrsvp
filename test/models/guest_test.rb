require 'test_helper'

class GuestTest < ActiveSupport::TestCase
  def setup
    @guests = Guest.all
  end

  test 'to_csv when to_csv all guests converted to csv format' do
    # arrange
    ex = "lastname,firstname,rsvp-code,dietaryreq,attending,responded\n"
    Guest.all.each do |g|
      exg = ''
      exg += "#{g.lastname},"
      exg += "#{g.firstname},"
      exg += "#{g.rsvp.nil? ? '""' : g.rsvp.code},"
      exg += "#{g.dietaryreq},"
      exg += "#{g.attending},"
      exg += "#{g.responded}"
      exg += "\n"
      ex +=  exg
    end

    # act
    actual = Guest.all.to_csv

    # assert
    assert_equal ex, actual
  end

  test 'add_from_csv when called then guests added to database' do
    # arrange
    data =
<<CSV
lastname,firstname
Doe,John
Doe,Jane
Lemon,Liz
CSV
    expected_count = 3

    # act
    actual = Guest.import_from_csv(data)

    # assert
    assert_equal(expected_count, actual.count)
  end
end
