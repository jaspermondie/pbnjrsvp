require 'prawn/measurement_extensions'

class RsvpsController < ApplicationController
  before_action :logged_in_user, except: [:search]
  before_action :set_rsvp, only: [:show, :edit, :update, :destroy, :replycard]

  # POST /rsvps/search
  def search
    lastname = params[:lastname]
    code = params[:code].blank? ? 'blank' : params[:code]
    redirect_to rsvp_edit_guests_path(code, lastname: lastname)
  end

  # GET /rsvps
  # GET /rsvps.json
  def index
    @rsvps = Rsvp.all
  end

  # GET /rsvps/replycards-pdf
  def replycards_pdf
    @rsvps = Rsvp.all
    pdf = generate_pdf(@rsvps)
    send_data pdf.render, filename: 'reply-cards.pdf', type: 'application/pdf'
  end

  # GET /rsvps/1/replycard
  def replycard
    pdf = generate_pdf([@rsvp])
    send_data pdf.render, filename: 'reply-cards.pdf', type: 'application/pdf'
    # send_data replycard.to_blob, type: 'image/png', filename: filename, disposition: 'inline'
  end

  # GET /rsvps/1
  # GET /rsvps/1.json
  def show
    respond_to do|format|
      format.html
    end
  end

  # GET /rsvps/new
  def new
    @rsvp = Rsvp.new
  end

  # GET /rsvps/1/edit
  def edit
  end

  # POST /rsvps
  # POST /rsvps.json
  def create
    @rsvp = Rsvp.new(rsvp_params)
    respond_to do |format|
      if @rsvp.save
        successful_create format
      else
        format.html { render :new }
        format.json { render json: @rsvp.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /rsvps/1
  # PATCH/PUT /rsvps/1.json
  def update
    respond_to do |format|
      if @rsvp.update(rsvp_params)
        notice = 'Rsvp was successfully updated.'
        format.html { redirect_to @rsvp, notice: notice }
        format.json { render :show, status: :ok, location: @rsvp }
      else
        format.html { render :edit }
        format.json { render json: @rsvp.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /rsvps/1
  # DELETE /rsvps/1.json
  def destroy
    @rsvp.destroy
    respond_to do |format|
      notice = 'Rsvp was successfully destroyed.'
      format.html { redirect_to rsvps_url, notice: notice }
      format.json { head :no_content }
    end
  end

  private

  def successful_create(format)
    notice = 'Rsvp was successfully created.'
    format.html { redirect_to @rsvp, notice: notice }
    format.json { render :show, status: :created, location: @rsvp }
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_rsvp
    @rsvp = Rsvp.find_by(code: params[:code])
  end

  # Never trust parameters from the scary internet,
  # only allow the white list through.
  def rsvp_params
    params.require(:rsvp).permit(:code)
  end

  def qr_gen(url)
    size = 8
    level = :h
    options = { size: 8, unit: 6 }

    qrcode = RQRCode::QRCode.new(url, size: size, level: level)
    svg    = RQRCode::Renderers::SVG.render(qrcode, options)

    qr = MiniMagick::Image.read(svg) { |i| i.format 'svg' }
    qr.format :png
  end

  def draw_code(img, code)
    x_pos, y_pos, point_size, x_spacing = 410, 365, 90, 88
    img.combine_options do |c|
      c.gravity 'Center'
      c.font 'app/assets/fonts/LiberationMono-Regular.ttf'
      c.pointsize point_size
      code.split('').each_with_index { |chr, i| c.draw "text #{x_pos + (x_spacing * i)},#{y_pos} '#{chr}'" }
      c.fill '#614E45'
    end
  end

  def draw_lastnames(img, lastnames)
    # list all unique last names in the reply card
    img.combine_options do |c|
      c.gravity 'NorthWest'
      c.font 'app/assets/fonts/Neoteric.ttf'
      c.pointsize 40
      c.draw "text 20,20 '#{lastnames.gsub("\'", '')}'"
      c.fill '#614E45'
    end
  end

  def merge_qr(img, qr)
    # time to place the qr code into the template
    img.composite(qr) do |c|
      c.channel 'A'
      c.alpha 'Activate'
      c.compose 'Over'
      c.gravity 'NorthEast'
      c.geometry '+20+20'
    end
  end

  def draw_replycard(url, lastnames, code)
    qr = qr_gen url

    # let's fill in the details
    template = 'app/assets/images/reply-card-v2.png'
    img = MiniMagick::Image.open(template)

    draw_code img, code
    draw_lastnames img, lastnames.join("\n")
    final_img = merge_qr img, qr

    final_img.combine_options do |c|
      c.rotate '-90'
    end
  end

  def single_reply_card(rsvp)
    return unless rsvp.guests.any?

    guests = rsvp.guests
    lastnames = guests.pluck(:lastname).uniq
    code = rsvp.code
    url = "www.jasperandphoebe.com/rsvps/#{code}/guests/edit?lastname=#{lastnames.min}"

    draw_replycard url, lastnames, code
  end

  def generate_pdf(rsvps)
    table = generate_reply_cards_table rsvps

    pdf = Prawn::Document.new page_size: 'A4', margin: 0.8.cm
    pdf.table table
    pdf
  end

  def generate_reply_cards_table(rsvps)
    replycards = []

    rsvps.each { |r| replycards << single_reply_card(r) }

    height = 12.5.cm
    table = []
    replycards.each_slice(2) do |group|
      g = []
      group.each { |rc| g << { image: rc.path, image_height: height } }
      table << g
    end

    table
  end
end
