class GuestsController < ApplicationController
  before_action :logged_in_user, except: [:update_all, :edit_all]
  before_action :set_guest, only: [:show, :edit, :update, :destroy]
  before_action :set_guests_from_rsvp, only: [:edit_all]

  # GET /guest/export
  def export
    send_data Guest.to_csv, filename: 'guests.csv'
  end

  # GET /post/prepare_import
  def browsefile
  end

  # POST /guest/import
  def import
    respond_to do |format|
      valid = Guest.create_from_csv params[:file]
      message = valid ? 'File was successfully imported.' :
                        'There were rows not added from csv file'
      type = valid ? :success : :warning
      format.html { redirect guests_path, message, type }
    end
  end

  # GET /guests/not_linked
  def guests_not_linked
    @guests = Guest.no_rsvp
  end

  # GET /guests/linked
  def guests_linked
    @guests = Guest.has_rsvp
  end

  # PATCH /guests/generate_rsvp
  def create_rsvp_code_for_guests
    respond_to do |format|
      if link_guests_to_rsvp_code
        message = 'Successfully created RSVP code for guests'
        format.html { redirect guests_not_linked_path, message, :success }
      else
        message = 'Couldn\'t link the guests to an RSVP code'
        format.html { redirect guests_not_linked_path, message, :warning }
      end
    end
  end

  # PATCH /guests/unlink_guests
  def unlink_guests
    respond_to do |format|
      if remove_rsvp_code_from_guests
        message = 'Successfully removed RSVP from guests'
        format.html { redirect guests_linked_path, message, :success }
      else
        message = 'Couldn\'t remove RSVP code the guests'
        format.html { redirect guests_linked_path, message, :warning }
      end
    end
  end

  # PATCH /rsvps/:rsvp/guests/edit
  def update_all
    respond_to do |format|
      if process_update_all
        format.html { redirect_to guests_responded_path }
        format.json { render guests_responded_path, status: :ok }
      else
        format.html { render :edit_all }
        format.json { render json: guest.errors, status: :unprocessable_entity }
      end
    end
  end

  # GET /rsvps/:rsvp/guests/edit
  def edit_all
  end

  # GET /guests
  # GET /guests.json
  def index
    if !params[:rsvp_code].blank?
      rsvp_code = params[:rsvp_code]
      @guests = Guest.by_rsvp(rsvp_code)
    else
      @guests = Guest.all
    end
  end

  # GET /guests/1
  # GET /guests/1.json
  def show
    @guest = Guest.find(params[:id])
  end

  # GET /guests/new
  def new
    @guest = Guest.new
  end

  # GET /guests/1/edit
  def edit
  end

  # POST /guests
  # POST /guests.json
  def create
    @guest = Guest.new(guest_params)

    respond_to do |format|
      if @guest.save
        format.html { redirect_to @guest, notice: 'Guest was successfully created.' }
        format.json { render :show, status: :created, location: @guest }
      else
        format.html { render :new }
        format.json { render json: @guest.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /guests/1
  # PATCH/PUT /guests/1.json
  def update
    respond_to do |format|
      if @guest.update(guest_params)
        format.html { redirect_to @guest, notice: 'Guest was successfully updated.' }
        format.json { render :show, status: :ok, location: @guest }
      else
        format.html { render :edit }
        format.json { render json: guest.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /guests/1
  # DELETE /guests/1.json
  def destroy
    @guest.destroy
    respond_to do |format|
      format.html { redirect_to guests_url, notice: 'Guest was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  def process_update_all
    guests_params_hash = guests_params

    @guests = Guest.where(id: guests_params_hash.keys)
    @rsvp_code = params.require(:rsvp_code)

    validate_rsvp_with_guests && Guest.update(guests_params_hash.keys,
                                              guests_params_hash.values)
  end

  def set_guests_from_rsvp
    verified_guest = Guest.match_code_and_lastname(params[:rsvp_code],
                                                   params[:lastname]).first
    if verified_guest && verified_guest.responded
      redirect root_url, "It looks like you've already responded to our RSVP", :warning
    elsif verified_guest
      @guests = Guest.by_rsvp(params[:rsvp_code])
    else
      redirect root_url, "We can't find a match for the lastname and RSVP Code", :warning
    end
  end

  def redirect(url, message, flash_type)
    flash[flash_type] = message
    redirect_to url
  end

  def guests_params
    g_prms_prmt = {}

    params.require(:guests).each do |id, guest_p|
      g_prms_prmt[id] = guest_p.permit(:firstname,
                                       :lastname,
                                       :dietaryreq,
                                       :attending,
                                       :responded)
    end

    g_prms_prmt
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_guest
    @guest = Guest.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def guest_params
    params.require(:guest).permit(:firstname,
                                  :lastname,
                                  :dietaryreq,
                                  :attending,
                                  :responded)
  end

  # let's validate that the guests are actually part of the rsvp code given
  def validate_rsvp_with_guests
    @guests.by_rsvp(@rsvp_code).count == Guest.by_rsvp(@rsvp_code).count
  end

  # Get the list of selected guests from the guest link list
  def selected_guests_from_list
    guest_ids = []

    params.require(:guests).each do |id, guest_p|
      selected = guest_p.permit(:selected)[:selected]
      guest_ids << id if selected == '1'
    end

    Guest.where(id: guest_ids)
  end

  # Links guests to rsvp code
  def link_guests_to_rsvp_code
    guests = selected_guests_from_list
    return unless guests.any? && guests.has_rsvp.empty?
    rsvp = Rsvp.generate_rsvp
    guests.update_all(rsvp_id: rsvp.id)
  end

  # Unlink guests to rsvp code
  def remove_rsvp_code_from_guests
    guests = selected_guests_from_list
    return unless guests.any? && guests.no_rsvp.empty?
    guests.update_all(rsvp_id: nil)
  end
end
