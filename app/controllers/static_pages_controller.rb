class StaticPagesController < ApplicationController
  def home
    @guest = Guest.new
    @rsvp = Rsvp.new
  end
end
