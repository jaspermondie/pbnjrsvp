json.array!(@events) do |event|
  json.extract! event, :id, :name, :eventtype, :eventdate, :rsvpdate
  json.url event_url(event, format: :json)
end
