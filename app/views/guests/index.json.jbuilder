json.array!(@guests) do |guest|
  json.extract! guest, :id, :firstname, :lastname, :dietaryreq, :attending
  json.url guest_url(guest, format: :json)
end
