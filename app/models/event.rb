class Event < ActiveRecord::Base
  has_many :guests
  validates :name, presence: true
end
