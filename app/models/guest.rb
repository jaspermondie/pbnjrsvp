class Guest < ActiveRecord::Base
  attr_accessor :code_search_text

  validates :lastname, presence: true

  belongs_to :event
  belongs_to :rsvp

  scope :by_rsvp,
        -> (code) { where(rsvp: Rsvp.find_by(code: code.upcase)) }

  scope :match_code_and_lastname,
        -> (code, lastname) { by_rsvp(code.upcase).where(lastname: lastname.titleize) }

  scope :no_rsvp, -> { where(rsvp: nil) }
  scope :has_rsvp, -> { where.not(rsvp: nil) }
  scope :attending, -> { where(attending: true) }
  scope :not_attending, -> { responded.where(attending: false) }
  scope :responded, -> { where(responded: true) }
  scope :not_responded, -> { where(responded: false) }

  def self.to_csv
    CSV.generate do |csv|
      csv << ['lastname', 'firstname', 'rsvp-code', 'dietaryreq', 'attending', 'responded']
      all.each do |guest|
        code = guest.rsvp.nil? ? '' : guest.rsvp.code
        csv << [guest.lastname, guest.firstname, code, guest.dietaryreq, guest.attending, guest.responded]
      end
    end
  end

  def self.import_from_csv(csv)
    valid_columns = %w(lastname firstname rsvp-code dietaryreq attending responded)
    rows_to_add = []

    CSV.parse(csv, headers: true) do |row|
      row_to_add = row.to_hash.slice(*valid_columns)
      rows_to_add << row.to_hash.slice(*valid_columns) if row_to_add.count > 0
    end

    rows_to_add
  end

  def self.create_from_csv(file)
    return false if file.blank?

    csv = File.read(file.path)
    imported_guests = Guest.import_from_csv(csv)
    guests_success = []
    imported_guests.each do |g|
      g_success = create_guest_with_rsvp_code g
      guests_success << g_success unless g_success.nil?
    end

    guests_success.count == imported_guests.count
  end

  def self.create_guest_with_rsvp_code(guesthash)
    gr = Guest.find_or_initialize_by(firstname: guesthash['firstname'],
                                     lastname: guesthash['lastname'])
    gr.rsvp_id = Rsvp.find_or_create_by(code: guesthash['rsvp-code']).id if gr.rsvp_id.nil?
    gr = set_extra_attributes(guesthash, gr)
    gr.save
    gr.valid? ? gr : nil
  end

  def self.set_extra_attributes(guesthash, gr)
    gr.dietaryreq = guesthash['dietaryreq']
    gr.attending = guesthash['attending']
    gr.responded = guesthash['responded']
    gr
  end
end
