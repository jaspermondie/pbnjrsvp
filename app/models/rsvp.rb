class Rsvp < ActiveRecord::Base
  validates :code, presence: true

  has_many :guests

  def self.generate_rsvp
    create!(code: generate_code)
  end

  def self.generate_code(length = 4)
    ([*('A'..'Z'), *('0'..'9')] - %w(0 1 I O)).sample(length).join
  end
end
